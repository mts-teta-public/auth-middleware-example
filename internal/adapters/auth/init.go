package auth

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"tasks/core/models"
	"tasks/pkg/infra/metrics"

	"github.com/juju/zaputil/zapctx"
	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.uber.org/zap"
)

type Adapter struct {
	authURL string

	client *http.Client
}

func New() *Adapter {
	return &Adapter{
		authURL: os.Getenv("AUTH_URL"),
		client: &http.Client{
			Transport: otelhttp.NewTransport(http.DefaultTransport),
		},
	}
}

func (a *Adapter) Verify(ctx context.Context, r *http.Request) error {
	ctx, span := metrics.Tracer.Start(ctx, "auth")
	defer span.End()

	authR, err := http.NewRequestWithContext(ctx, http.MethodPost, fmt.Sprintf("%s/verify", a.authURL), http.NoBody)
	if err != nil {
		return err
	}

	resp, err := a.client.Do(authR)
	if err != nil {
		return err
	}

	if resp.StatusCode == http.StatusOK {
		return nil
	}

	switch resp.StatusCode {
	case http.StatusForbidden:
		return models.ErrForbidden
	default:
		zapctx.Error(ctx, "auth request failed", zap.Error(err))
		return fmt.Errorf("unexpected error")
	}
}
