package models

import "fmt"

var ErrForbidden = fmt.Errorf("access denied")