package main

import (
	"context"
	"errors"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"tasks/core/models"
	"tasks/internal/adapters/auth"
	"tasks/pkg/infra/logger"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), syscall.SIGTERM, os.Interrupt)
	defer cancel()

	l, err := logger.New()
	if err != nil {
		log.Fatalf("logger initialization failed: %s", err.Error())
	}

	authAdapter := auth.New()

	router := gin.Default()
	router.Use(authMiddleware(authAdapter))

	var wg sync.WaitGroup
	wg.Add(1)
	errCh := make(chan error)

	go func() {
		errCh <- router.Run(":3001")
	}()

	select {
	case err := <-errCh:
		l.Fatal("server start failed", zap.String("err", err.Error()))
	case <-ctx.Done():
		l.Fatal("app shutdown")
	}
}

type AuthAdapter interface {
	Verify(ctx context.Context, r *http.Request) error
}

func authMiddleware(a AuthAdapter) gin.HandlerFunc {
	fn := func(c *gin.Context) {
		err := a.Verify(c, c.Request)
		switch {
		case errors.Is(err, models.ErrForbidden):
			c.AbortWithStatus(http.StatusForbidden)
			return
		default:
			c.AbortWithError(http.StatusInternalServerError, err)
			return
		}

		c.Next()
	}

	return fn
}
